//variables
var app1 = new Vue({
  el: '#app1',
  data: {
    greeting: 'Hello World!',
  },
})

//Conditional Statements
var app2 = new Vue({
  el: '#app2',
  data: {
    greeting: 'Hello Friend',
    isFriend: false,
  },
})

//User Input
var app3 = new Vue({
  el: '#app3',
  data: {
    greeting: 'Hello',
    name: '',
  },
})

//Loops
var app4 = new Vue({
  el: '#app4',
  data: {
    shoppingList: ['Eggs', 'Milk', 'Bread'],
  },
})

//Methods
var app5 = new Vue({
  el: '#app5',
  data: {
    count: 0,
  },
  methods: {
    add: function () {
      this.count = this.count + 1
    },
  },
})
