//variables
var app1 = new Vue({
  el: '#app1',
  data: {},
})

//Conditional Statements
var app2 = new Vue({
  el: '#app2',
  data: {},
})

//User Input
var app3 = new Vue({
  el: '#app3',
  data: {},
})

//Loops
var app4 = new Vue({
  el: '#app4',
  data: {},
})

//Methods
var app5 = new Vue({
  el: '#app5',
  data: {},
})
